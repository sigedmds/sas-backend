
# Backend

##  Acerca de y objetivo :scroll:
En este repositorio se engloban todos los microservicios que agruparán la lógica propia del negocio y las validaciones inherentes a las mismas. Contiene todos los posibles end-points del sistema para, entre otras, cosas acceder a la base de datos.

Se utilizan las siguientes tecnologías:

 - .Net Core
 - Dapper
 

##  Configuración inicial :wrench:
Es necesario:

 - Contar con algún IDE como Visual Studio (recomendado) 2019 en adelante o Visual Studio Code.
 - Contar con las SDK de net core 2.2 y 3.0 (ver "Configuración de proyecto
" en la wiki)
 - Contar con docker (Linux o Windows), el engine completo. Este punto es opcional para desarrollar.

## Ejecución y despliegue :hammer:
### Ejecución

Si se utiliza Visual Studio 2019+ (se puede descargar la version Community de forma gratuita desde la web oficial), se deberá configurar el inicio múltiple para todos los proyectos y bibliotecas de clase que se quieran iniciar (dentro de las "propiedades de la solución").

Al ejecutar el proyecto, se abrirá una nueva ventana con Swagger incluido para ver los end-points de cada microservicio contenido en el repositorio, con sus URIs, parámetros de entrada, tipo de request y en caso de ser un POST, un JSON de ejemplo con los atributos que se mapearán de forma automática al llamar a dicho end-point.

El puerto donde se levanta el microservicio se configura en el archivo `launchSettings.json` dentro de las propiedades de cada una de las apis/microservicios.




Si se opta por utilizar Visual Studio Code, se deberá instalar la extensión de C# a través de la sección "Extensiones" dentro del mismo. Existe una extensión oficial de Microsoft para este fin.
Para ejecutar la aplicación desde Visual Code, en la terminal se ingresa el siguiente comando
 - `dotnet run`

 Para poder depurarlo al igual que en Visual Studio, tendremos un paso extra que será ir a la opción del menú superior del programa a "`Ver--> Paleta de comandos`", lo cual abrirá un desplegable y seleccionaremos la opción `.NET: Generate Assets for Build and Debug`, lo cual generará los archivos en formato JSON necesarios para compilar, ejecutar y depurar el proyecto.

 Una vez hecho estos pasos, queda presionar la tecla `F9` para lanzar el proyecto en modo debug.

### Ejecución dockerizada
Ejecutar el siguiente comando parado sobre la carpeta raíz del proyecto a la altura de `docker-compose.yml` :
<br /><br />
`docker-compose up --build -d`


## Referencias :books:
Recursos: <br />
[Ejecutar la aplicación en VS Code](https://www.campusmvp.es/recursos/post/visual-studio-code-como-preparar-un-entorno-de-trabajo-para-net-core.aspx)<br />
[Docker](https://www.docker.com/get-started)<br />
[Dapper](https://dapper-tutorial.net/dapper)<br />
[Visual Studio](https://visualstudio.microsoft.com/es/vs/)<br />