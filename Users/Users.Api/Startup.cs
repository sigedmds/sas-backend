using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Middlewares.Cors;
using Core.Middlewares.Documentador;
using Core.Middlewares.ErrorHandling;
using Core.Middlewares.Extensions;
using Core.Middlewares.Validations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Users.Application.Services;
using Users.Domain.Interfaces;
using Users.Domain.Interfaces.IRepositories;
using Users.Domain.Interfaces.IServices;
using Users.Infraestructure;
using Users.Infraestructure.Repositories;

namespace Users.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureCors();

            services.ConfigureAutoMapper(AppDomain.CurrentDomain);

            services.ConfigueSwagger("UsersApi");

            ConfigureInjection(services);

            services.AddControllers(config =>
            {
                /*
                                var policy = new AuthorizationPolicyBuilder()
                                    .RequireAuthenticatedUser()
                                    .Build();
                                config.Filters.Add(new AuthorizeFilter(policy));*/
                config.Filters.Add(typeof(ValidateModelFilter));
            });

            services.AddSwaggerGen();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(Policies.AllowAllPolicy);

            app.UseHttpsRedirection();

            app.UseExceptionHandlerMiddleware();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Api Usuarios");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private static void ConfigureInjection(IServiceCollection services)
        {/*
            services.BindAll<IService>(AppDomain.CurrentDomain);
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.BindAll<IRepository>(AppDomain.CurrentDomain);*/
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IUserService, UserService>();

        }
    }
}
