﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Users.Domain.Interfaces;

namespace Users.Infraestructure
{
    public class RepositoryBase
    {
        protected readonly IUnitOfWork _uow;
        public RepositoryBase(IUnitOfWork unitOfWork)
        {
            _uow = unitOfWork;
        }

        protected SqlTransaction BeginTransaction()
        {
            return (SqlTransaction)_uow.Begin();
        }

        #region Helpers
        protected void ExecuteCommand(Action<SqlTransaction> task)
        {
            var transaction = BeginTransaction();
            if (transaction.Connection.State == ConnectionState.Closed)
                throw new Exception("Connection must be open.");

            task(transaction);
        }

        protected T ExecuteCommand<T>(Func<SqlTransaction, T> task)
        {
            var transaction = BeginTransaction();
            if (transaction.Connection.State == ConnectionState.Closed)
                throw new Exception("Connection must be open.");

            return task(transaction);
        }

        protected Task<T> ExecuteCommandAsync<T>(Func<SqlTransaction, Task<T>> task)
        {
            var transaction = BeginTransaction();
            if (transaction.Connection.State == ConnectionState.Closed)
                throw new Exception("Connection must be open.");

            return task(transaction);
        }

        #endregion
    }
}