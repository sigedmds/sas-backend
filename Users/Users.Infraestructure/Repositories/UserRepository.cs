﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Dapper;
using Users.Domain.Entities;
using Users.Domain.Interfaces;
using Users.Domain.Interfaces.IRepositories;

namespace Users.Infraestructure.Repositories
{
    public class UserRepository : RepositoryBase, IUserRepository
    {
        public UserRepository(IUnitOfWork uow) : base(uow)
        {

        }

        public async Task<User> Get(int id)
        {
            return (await ExecuteCommandAsync(tx => tx.Connection.QueryFirstOrDefaultAsync<User>(
                "SP_GET_USER_BY_ID",

                new { UserId = id},
                tx,
                commandType: CommandType.StoredProcedure)));
            /* var tx = BeginTransaction();
             return( await tx.Connection.QueryFirstAsync<User>(
                 "SP_GET_USER_BY_ID", 
                 new { UserId = id },
                 tx, 
                 commandType: CommandType.StoredProcedure)).FirstOrDefault();*/
        }

        public ICollection<User> GetUsers()
        {
            return ExecuteCommand(tx => tx.Connection.Query<User, object, object, Role, User>(
                "SP_GET_USERS", 
                map: (u, p, c, pr) =>
                {
                    u.Role = pr;
                    return u;
                }, new { EmpresaId = 1, UserId = 1 },
                splitOn: "Id,Id,Id,Id",
                transaction: tx,
                commandType: CommandType.StoredProcedure)) as ICollection<User>;
        }
    }
}