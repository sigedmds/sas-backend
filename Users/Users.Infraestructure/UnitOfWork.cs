﻿using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Users.Domain.Interfaces;

namespace Users.Infraestructure
{
    public class UnitOfWork: IUnitOfWork
    {
        private readonly IDbConnection _connection;
        private IDbTransaction _transaction;

        public UnitOfWork(IConfiguration configuration)
        {
            _connection = new SqlConnection(configuration.GetConnectionString("Db"));
        }

        public IDbTransaction Begin()
        {
            if (_transaction == null)
            {
                if (_connection.State == ConnectionState.Closed)
                    _connection.Open();
                _transaction = _connection.BeginTransaction();
            }

            return _transaction;
        }

        public void SaveChanges()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                Dispose();
            }
        }

        public void Rollback()
        {
            _transaction.Rollback();
            Dispose();
        }

        public void Dispose()
        {
            _transaction?.Dispose();
            _transaction = null;
            _connection.Close();
        }
    }
}