﻿using System;
using System.Data;

namespace Users.Domain.Interfaces
{
    public interface IUnitOfWork: IDisposable
    {
        IDbTransaction Begin();
        void SaveChanges();
        void Rollback();
    }
}