﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Users.Domain.Entities;

namespace Users.Domain.Interfaces.IRepositories
{
    public interface IUserRepository: IRepository
    {
        ICollection<User> GetUsers();
        Task<User> Get(int id);
    }
}