﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Users.Domain.Entities;

namespace Users.Domain.Interfaces.IServices
{
    public interface IUserService: IService
    {
        IEnumerable<User> GetUsers();
        Task<User> Get(int id);
    }
}