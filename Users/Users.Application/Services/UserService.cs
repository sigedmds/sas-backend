﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Users.Domain.Entities;
using Users.Domain.Interfaces.IRepositories;
using Users.Domain.Interfaces.IServices;

namespace Users.Application.Services
{
	public class UserService : IUserService
	{
		private readonly IUserRepository _userRepository;

		public UserService(IUserRepository userRepository)
		{
			_userRepository = userRepository;
		}

		public IEnumerable<User> GetUsers()
		{
			return new List<User>{
				new User
                {
					Id = 1,
					Name = "matias",
					Username = "mata",
					Role = new Role
                    {
						Name = "admin",
						Id = 1
                    }
                }
            };
			//return _userRepository.GetUsers();
		}

		public async Task<User> Get(int id)
		{
			return await _userRepository.Get(id);
		}
	}
}