﻿namespace ApiGateway
{
	public class WebProtocolSettings
	{
		public string Url { get; set; }
		public string Protocol { get; set; }
		public string Host { get; set; }
		public int Port { get; set; }
	}
}
