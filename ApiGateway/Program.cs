using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ApiGateway
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var envName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"appsettings.{envName}.json",
                    true)
                .AddCommandLine(args)
                .Build();

            var webSettings = new WebProtocolSettings();
            builder.GetSection("WebProtocolSettings").Bind(webSettings);

            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                        .UseUrls($"{webSettings.Protocol}{webSettings.Host}:{webSettings.Port}")
                        //.UseKestrel()
                        .UseStartup<Startup>();
                })
                .ConfigureAppConfiguration(
                    (context, config) =>
                    {
                        config.AddJsonFile($"ocelot.{envName}.json", true);
                    })
                .ConfigureLogging(logging => logging.AddConsole());
        }
    }
}
