﻿using System.Collections.Generic;

namespace Core.Middlewares.ResponsesError
{
    public abstract class ResponseError
    {
        public string Titulo { get; set; }

        public class Production : ResponseError
        {
            public IList<string> Mensajes { get; set; }
        }

        public class NoProduction : ResponseError
        {
            public string Description { get; set; }

            public string StackTrace { get; set; }

            public IList<string> Mensajes { get; set; }

            public string Origen { get; set; }
        }
    }
}
