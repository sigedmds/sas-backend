﻿using System.Collections.Generic;

namespace Core.Middlewares.ResponsesError
{
    public abstract class ResponseModelStateError
    {
        public string Mensaje { get; set; }

        public class Production : ResponseModelStateError
        {
        }

        public class NoProduction : ResponseModelStateError
        {
            public string Origen { get; set; }

            public string Descripcion { get; set; }
        }
    }
}
