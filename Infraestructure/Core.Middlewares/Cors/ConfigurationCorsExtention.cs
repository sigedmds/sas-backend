﻿using Microsoft.Extensions.DependencyInjection;

namespace Core.Middlewares.Cors
{
    public static class ConfigurationCorsExtention
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options => options.AddPolicy(Policies.AllowAllPolicy,
                builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                }));
        }
    }
}