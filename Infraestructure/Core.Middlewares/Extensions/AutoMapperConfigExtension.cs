﻿using System;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Middlewares.Extensions
{
    public static class AutoMapperConfigExtension
    {
        public static void ConfigureAutoMapper(this IServiceCollection services, AppDomain appDomain)
        {
            var assemblies = appDomain.GetAssemblies();
            services.AddAutoMapper(assemblies);
            //MappingConfig.RegisterMappings();
        }
    }
}
