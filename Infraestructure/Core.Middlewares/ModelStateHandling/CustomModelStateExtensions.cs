﻿using Core.Middlewares.ModelStateHandling;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Core.Middlewares.ErrorHandling
{
    public static class CustomModelStateExtensions
    {
        public static void ConfigureModelStateMiddleware(this IServiceCollection services)
        {
            services.AddMvcCore(
                options => options.Filters.Add(typeof(ModelStateMiddleware))        
            );

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.AddScoped<ModelStateMiddleware>();
        }
    }
}
