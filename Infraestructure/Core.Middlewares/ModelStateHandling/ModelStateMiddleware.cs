﻿using Core.Common.ErrorsAndExceptions;
using Core.Middlewares.ResponsesError;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using Microsoft.Extensions.Hosting;

namespace Core.Middlewares.ModelStateHandling
{
    public class ModelStateMiddleware : ActionFilterAttribute
    {
        private readonly IWebHostEnvironment _env;

        public ModelStateMiddleware(IWebHostEnvironment env)
        {
            _env = env;
        }

        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {

            if (actionContext.ModelState.IsValid == false)
            {
                ResponseError re;
                var ce = new NoValidException();
                foreach (var key in actionContext.ModelState.Keys)
                {

                    foreach (var error in actionContext.ModelState[key].Errors)
                    {

                        var keyName = key;

                        if (actionContext.ActionArguments.Count == 1)
                        {
                            var nameVar = actionContext.ActionArguments.Keys.FirstOrDefault();
                            keyName = key.Replace(string.Format("{0}.", nameVar), "");
                        }

                        if (string.IsNullOrEmpty(error.ErrorMessage) && error.Exception == null)
                            continue;
                        if (!_env.IsProduction())
                            ce.AgregarMensaje(string.IsNullOrEmpty(error.ErrorMessage) ? "Error inesperado" : $"{keyName}: {error.ErrorMessage}");
                        else
                            ce.AgregarMensaje(string.IsNullOrEmpty(error.ErrorMessage) ? "Error inesperado" : error.ErrorMessage);
                    }
                }

                if (_env.IsProduction())
                {
                    re = new ResponseError.Production
                    {
                        Mensajes = ce?.Errores,
                        Titulo = ce.Titulo
                    };
                }
                else
                {
                    re = new ResponseError.NoProduction
                    {
                        Titulo = ce.Titulo,
                        Mensajes = ce?.Errores
                    };
                }
                actionContext.Result = new BadRequestObjectResult(re);
            }
        }
    }
}