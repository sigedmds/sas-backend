﻿using Microsoft.AspNetCore.Builder;

namespace Core.Middlewares.ErrorHandling
{
    public static class CustomErrorExcepcionExtensions
    {
        public static IApplicationBuilder UseCustomErrorExcepcion(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CustomExcepcionMiddleware>();
        }
    }
}
