﻿using Core.Middlewares.ResponsesError;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace Core.Middlewares.ErrorHandling
{
    public class CustomExcepcionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IWebHostEnvironment _env;

        public CustomExcepcionMiddleware(RequestDelegate next, IWebHostEnvironment env)
        {
            _next = next;
            _env = env;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (Exception ex)
            {
                await HandleExcepcionAsync(context, ex);
            }
        }

        public async Task HandleExcepcionAsync(HttpContext context, Exception ex)
        {
            var response = context.Response;
            var customExcepcion = ex as Common.ErrorsAndExceptions.ErrorException;
            ResponseError re;

            if (null == customExcepcion)
            {
                customExcepcion = new Common.ErrorsAndExceptions.ErrorException("Error", null, null, (int)HttpStatusCode.InternalServerError);
                customExcepcion.AgregarMensaje("Ha ocurrido un error inespeado");
            }

            if (_env.IsProduction())
            {
                re = new ResponseError.Production
                {
                    Titulo = customExcepcion.Titulo,
                    Mensajes = customExcepcion?.Errores
                };
            }
            else
            {
                re = new ResponseError.NoProduction
                {
                    Titulo = customExcepcion.Titulo,
                    Mensajes = customExcepcion?.Errores,
                    Description = ex.Message,
                    StackTrace = ex.StackTrace
                };
            }

            response.ContentType = "application/json";
            response.StatusCode = customExcepcion.StatusCode;
            await response.WriteAsync(
                JsonConvert.SerializeObject(re, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }));
        }
    }
}
