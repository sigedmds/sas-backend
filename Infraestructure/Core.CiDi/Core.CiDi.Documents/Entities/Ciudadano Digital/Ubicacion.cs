﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.CiDi.Documents.Entities.Ciudadano_Digital
{
    public class Ubicacion
    {
        public int IdUbicacionFisica { get; set; }
        public String NombreUbicacionFisica { get; set; }
    }
}
