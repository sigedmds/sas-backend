﻿using System;

namespace Core.CiDi.Documents.Entities.CDDResponse
{
    public class CDDResponse
    {
        public String Codigo_Resultado { get; set; }
        public String Detalle_Resultado { get; set; }
    }
}