﻿using System.Collections.Generic;

namespace Core.CiDi.Documents.Entities.CDDResponse
{
    public class CDDResponseListaDocumentacion : CDDResponse
    {
        public List<MetadataDocumentacionCDD> ListaMetadataCDD { get; set; }

        public CDDResponseListaDocumentacion()
        {
            ListaMetadataCDD = new List<MetadataDocumentacionCDD>();
        }
    }
}