﻿using System;

namespace Core.CiDi.Documents.Entities.CDDResponse
{
    public class CDDResponseSLogin : CDDResponse
    {
        /// <summary>
        /// Llave BLOB pública.
        /// </summary>
        public String Llave_BLOB_Login { get; set; }
    }
}