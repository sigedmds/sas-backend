﻿using Core.CiDi.Documents.Entities.Auditoria;
using System.Collections.Generic;

namespace Core.CiDi.Documents.Entities.CDDResponse
{
    public class CDDResponseAuditoriaGeneral : CDDResponse
    {
        /// <summary>
        /// Listado de auditoria general sobre un documento digitalizado.
        /// </summary>
        public List<AuditoriaGeneral> Lista_Auditoria_General { get; set; }

        public CDDResponseAuditoriaGeneral()
        {
            Lista_Auditoria_General = new List<AuditoriaGeneral>();
        }
    }
}