﻿using System;

namespace Core.CiDi.Documents.Entities.CDDResponse
{
    public class CDDResponseEliminacion : CDDResponse
    {
        /// <summary>
        /// Identificador de documento.
        /// </summary>
        public Int32 Id_Documento { get; set; }
    }
}