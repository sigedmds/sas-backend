﻿namespace Core.CiDi.Documents.Entities.CDDResponse
{
    public class CDDResponseLogin : CDDResponse
    {
        public byte[] Llave_BLOB_Login { get; set; }
    }
}