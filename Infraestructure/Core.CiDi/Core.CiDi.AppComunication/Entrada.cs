﻿namespace AppComunicacion
{
  internal class Entrada
  {
    public int IdAplicacion { get; set; }

    public string Contrasenia { get; set; }

    public string TokenValue { get; set; }

    public string TimeStamp { get; set; }

    public int IdDocumento { get; set; }

    public string Cuil { get; set; }

    public string HashCookie { get; set; }
  }
}
