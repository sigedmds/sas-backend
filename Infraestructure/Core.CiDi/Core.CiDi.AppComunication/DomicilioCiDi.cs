﻿namespace AppComunicacion
{
  internal class DomicilioCiDi
  {
    public string Provincia { get; set; }

    public string Departamento { get; set; }

    public string Localidad { get; set; }

    public string Barrio { get; set; }

    public string Calle { get; set; }

    public string Altura { get; set; }

    public string CodigoPostal { get; set; }

    public string Piso { get; set; }

    public string Depto { get; set; }

    public string Torre { get; set; }
  }
}
