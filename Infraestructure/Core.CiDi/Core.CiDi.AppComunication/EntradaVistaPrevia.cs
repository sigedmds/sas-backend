﻿using System.Collections.Generic;

namespace AppComunicacion
{
  internal class EntradaVistaPrevia : Entrada
  {
    public Dictionary<int, int> DiccionarioDocumentos { get; set; }

    public EntradaVistaPrevia()
    {
      this.DiccionarioDocumentos = new Dictionary<int, int>();
    }
  }
}
