﻿namespace AppComunicacion.ApiModels
{
  public class TipoMovimientoHistorico
  {
    public int Id { get; set; }

    public string Descripcion { get; set; }
  }
}
