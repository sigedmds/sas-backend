﻿namespace AppComunicacion.ApiModels
{
  public class TipoCaracteristica
  {
    public string IdTipoCaracteristica { get; set; }

    public string Descripcion { get; set; }

    public bool MultipleValor { get; set; }

    public string Punto { get; set; }

    public string Detalle { get; set; }

    public OrigenCaracteristica Origen { get; set; }

    public TipoCaracteristica()
    {
      this.Origen = new OrigenCaracteristica();
    }
  }
}
