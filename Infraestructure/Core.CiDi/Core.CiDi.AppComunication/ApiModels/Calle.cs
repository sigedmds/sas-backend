﻿namespace AppComunicacion.ApiModels
{
  public class Calle
  {
    public int IdCalle { get; set; }

    public string Nombre { get; set; }

    public string id { get; set; }

    public string text { get; set; }

    public Localidad Localidad { get; set; }
  }
}
