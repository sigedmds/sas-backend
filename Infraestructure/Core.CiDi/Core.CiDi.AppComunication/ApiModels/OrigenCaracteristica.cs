﻿namespace AppComunicacion.ApiModels
{
  public class OrigenCaracteristica
  {
    public string IdOrigenCaracteristica { get; set; }

    public string Descripcion { get; set; }

    public TipoDato TipoDato { get; set; }

    public OrigenCaracteristica()
    {
      this.TipoDato = new TipoDato();
    }
  }
}
