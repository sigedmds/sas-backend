﻿namespace AppComunicacion.ApiModels
{
  public class EstadoGrupo
  {
    public string Id { get; set; }

    public string Nombre { get; set; }
  }
}
