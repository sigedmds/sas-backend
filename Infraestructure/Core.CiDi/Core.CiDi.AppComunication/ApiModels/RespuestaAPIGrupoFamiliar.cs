﻿namespace AppComunicacion.ApiModels
{
  public class RespuestaAPIGrupoFamiliar
  {
    public PersonaUnica Persona { get; set; }

    public GrupoFamiliar Grupo { get; set; }
  }
}
