﻿namespace AppComunicacion.ApiModels
{
  public class IntegranteGrupo : PersonaUnica
  {
    public long IdGrupo { get; set; }

    public EstadoGrupo Estado { get; set; }

    public bool Pivote { get; set; }
  }
}
