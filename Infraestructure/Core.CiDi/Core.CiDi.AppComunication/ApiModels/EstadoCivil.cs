﻿namespace AppComunicacion.ApiModels
{
  public class EstadoCivil
  {
    public string Id { get; set; }

    public string Nombre { get; set; }
  }
}
