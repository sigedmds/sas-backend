﻿namespace AppComunicacion.ApiModels
{
  public class Caracteristica
  {
    public string IdCaracteristica { get; set; }

    public string Descripcion { get; set; }

    public bool ValorUsuario { get; set; }

    public string Valor { get; set; }

    public TipoCaracteristica TipoCaracteristica { get; set; }

    public Caracteristica()
    {
      this.TipoCaracteristica = new TipoCaracteristica();
    }
  }
}
