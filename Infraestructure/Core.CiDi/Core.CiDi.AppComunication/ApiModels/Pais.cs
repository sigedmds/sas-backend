﻿namespace AppComunicacion.ApiModels
{
  public class Pais
  {
    public string IdPais { get; set; }

    public string Nombre { get; set; }

    public string Nacionalidad { get; set; }
  }
}
