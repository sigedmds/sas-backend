﻿namespace AppComunicacion.ApiModels
{
  public class Provincia
  {
    public string IdProvincia { get; set; }

    public string Nombre { get; set; }

    public Pais Pais { get; set; }
  }
}
