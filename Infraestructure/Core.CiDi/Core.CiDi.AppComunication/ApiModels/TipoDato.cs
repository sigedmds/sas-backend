﻿namespace AppComunicacion.ApiModels
{
  public class TipoDato
  {
    public string IdTipoDato { get; set; }

    public string NombreTipoDato { get; set; }
  }
}
