﻿namespace AppComunicacion.ApiModels
{
  public class CaracteristicasPersona
  {
    public Caracteristica Trabaja { get; set; }

    public Caracteristica Trabajo { get; set; }

    public Caracteristica OtrosIngresos { get; set; }

    public Caracteristica CoberturaMedica { get; set; }

    public Caracteristica Discapacidad { get; set; }

    public Caracteristica Certificado { get; set; }

    public Caracteristica GastosMedicamento { get; set; }

    public Caracteristica GastosTelefonico { get; set; }

    public Caracteristica GastosTv { get; set; }

    public Caracteristica GastosInternet { get; set; }

    public Caracteristica GastosObraSocial { get; set; }

    public Caracteristica EnfermedadCronica { get; set; }

    public Caracteristica NivelEscolaridad { get; set; }

    public Caracteristica TipoEstablecimiento { get; set; }

    public Caracteristica NivelAlcanzado { get; set; }

    public Caracteristica TerminoNivel { get; set; }

    public Caracteristica EnCurso { get; set; }
  }
}
