﻿namespace AppComunicacion.ApiModels
{
  public class TipoDomicilio
  {
    public int IdTipoDomicilio { get; set; }

    public string Nombre { get; set; }
  }
}
