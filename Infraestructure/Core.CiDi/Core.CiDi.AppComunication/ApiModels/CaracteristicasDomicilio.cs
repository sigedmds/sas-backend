﻿namespace AppComunicacion.ApiModels
{
  public class CaracteristicasDomicilio
  {
    public Caracteristica TipoPropiedad { get; set; }

    public Caracteristica TipoTecho { get; set; }

    public Caracteristica TipoPared { get; set; }

    public Caracteristica TipoPiso { get; set; }

    public Caracteristica TipoHabitacion { get; set; }

    public Caracteristica TipoBano { get; set; }

    public Caracteristica TipoAgua { get; set; }

    public Caracteristica TipoElectricidad { get; set; }

    public Caracteristica TipoCoccion { get; set; }

    public Caracteristica TipoDomicilio { get; set; }

    public Caracteristica Ubicacion { get; set; }
  }
}
