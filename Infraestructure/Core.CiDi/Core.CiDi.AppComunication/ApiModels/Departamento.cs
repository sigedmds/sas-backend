﻿namespace AppComunicacion.ApiModels
{
  public class Departamento
  {
    public int IdDepartamento { get; set; }

    public string Nombre { get; set; }

    public Provincia Provincia { get; set; }
  }
}
