﻿namespace AppComunicacion.ApiModels
{
  public class Complejo
  {
    public int IdComplejo { get; set; }

    public string Nombre { get; set; }

    public Barrio Barrio { get; set; }
  }
}
