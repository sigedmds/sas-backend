﻿namespace AppComunicacion.ApiModels
{
  public class TipoDocumento
  {
    public string Id { get; set; }

    public string Nombre { get; set; }
  }
}
