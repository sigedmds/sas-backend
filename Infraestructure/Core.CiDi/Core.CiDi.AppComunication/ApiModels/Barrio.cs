﻿namespace AppComunicacion.ApiModels
{
  public class Barrio
  {
    public int IdBarrio { get; set; }

    public string Nombre { get; set; }

    public string CodigoPostal { get; set; }

    public Localidad Localidad { get; set; }
  }
}
