﻿namespace AppComunicacion.ApiModels
{
  public class EntradaHistoricoGrupos
  {
    public Aplicacion Aplicacion { get; set; }

    public TipoMovimientoHistorico TipoMovimiento { get; set; }
  }
}
