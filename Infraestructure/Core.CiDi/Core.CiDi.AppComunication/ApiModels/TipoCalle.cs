﻿namespace AppComunicacion.ApiModels
{
  public class TipoCalle
  {
    public int IdTipoCalle { get; set; }

    public string Nombre { get; set; }
  }
}
