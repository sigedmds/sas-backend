﻿using System.Collections.Generic;

namespace AppComunicacion.ApiModels
{
  public class GrupoFamiliar
  {
    public long? IdGrupo { get; set; }

    public Domicilio Domicilio { get; set; }

    public IList<IntegranteGrupo> Integrantes { get; set; }

    public EntradaHistoricoGrupos UltimaModificacionGrupo { get; set; }

    public GrupoFamiliar()
    {
      this.Integrantes = (IList<IntegranteGrupo>) new List<IntegranteGrupo>();
    }
  }
}
