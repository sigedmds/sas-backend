﻿namespace AppComunicacion.ApiModels
{
  public class Sexo
  {
    public string IdSexo { get; set; }

    public string Nombre { get; set; }

    public string NombreCorto
    {
      get
      {
        return this.IdSexo == "01" ? "M" : (this.IdSexo == "02" ? "F" : string.Empty);
      }
    }

    public Sexo()
    {
    }

    public Sexo(string idSexo)
    {
      this.IdSexo = idSexo;
    }

    public Sexo(string idSexo, string tipo)
    {
      this.IdSexo = idSexo;
      this.Nombre = tipo;
    }
  }
}
