﻿namespace AppComunicacion.ApiModels
{
  public class Localidad
  {
    public int IdLocalidad { get; set; }

    public string Nombre { get; set; }

    public string Tipo { get; set; }

    public double? Latitud { get; set; }

    public double? Longitud { get; set; }

    public string CodigoPostal { get; set; }

    public Departamento Departamento { get; set; }
  }
}
