﻿namespace AppComunicacion
{
  internal class RespuestaDoc : Respuesta
  {
    public Documentacion Documentacion { get; set; }

    public RespuestaDoc()
    {
      this.Documentacion = new Documentacion();
    }
  }
}
