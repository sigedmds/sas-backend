﻿using System.Collections.Generic;

namespace AppComunicacion
{
  internal class Respuesta
  {
    public string Resultado { get; set; }

    public string CodigoError { get; set; }

    public int Cantidad { get; set; }

    public string ExisteUsuario { get; set; }

    public string SesionHash { get; set; }

    public List<UsuarioCiDi> Usuarios { get; set; }
  }
}
