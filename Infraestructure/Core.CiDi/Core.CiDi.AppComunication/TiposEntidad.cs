﻿namespace AppComunicacion
{
  public enum TiposEntidad
  {
    PERSONA = 1,
    GRUPO_FAMILIAR = 2,
    DOMICILIO = 3,
    PERSONA_CUIL = 4,
  }
}
