﻿namespace AppComunicacion
{
  public enum RolesAPIDomicilios
  {
    CONSULTAR_DATOS_BASICOS = 30, // 0x0000001E
    CONSULTAR_CON_CARACTERISTICAS = 31, // 0x0000001F
    CONSULTAR_DOMICILIO_GEN = 35, // 0x00000023
  }
}
