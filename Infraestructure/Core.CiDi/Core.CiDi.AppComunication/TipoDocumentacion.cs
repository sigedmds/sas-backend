﻿namespace AppComunicacion
{
  internal class TipoDocumentacion
  {
    public int IdTipo { get; set; }

    public string Descripcion { get; set; }

    public string Acumulable { get; set; }
  }
}
