﻿namespace AppComunicacion
{
  public class Configuracion
  {
    private string _entorno;
    public const string CiDiConfigName = "AppComunicacion";

    public string Entorno
    {
      get
      {
        return this._entorno;
      }
      set
      {
        this._entorno = value == "produccion" || value == "desarrollo" ? value : string.Empty;
      }
    }

    public string AppId { get; set; }

    public string AppPass { get; set; }

    public string AppKey { get; set; }

    public string SesionHash { get; set; }
  }
}
