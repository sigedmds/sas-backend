﻿namespace AppComunicacion
{
    internal class CiDiConfiguration
    {
        private string _entorno;
        public const string CiDiConfigName = "CiDiConfiguration";

        public string Entorno
        {
            get
            {
                return this._entorno;
            }
            set
            {
                this._entorno = value == "produccion" || value == "desarrollo" ? value : string.Empty;
            }
        }

        public string AppId { get; set; }

        public string AppPass { get; set; }

        public string AppKey { get; set; }

        public string SesionHash { get; set; }

        public string ConnectionString { get; set; }

        public string SchemaName { get; set; }

        public bool ShowErrorMessage { get; set; }
    }
}
