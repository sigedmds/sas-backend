﻿namespace AppComunicacion
{
  public class ParametrosComunicacion
  {
    public int IdApp { get; set; }

    public string TokenSesion { get; set; }

    public string TimeStamp { get; set; }
  }
}
