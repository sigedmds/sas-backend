﻿namespace AppComunicacion
{
  internal class EntradaAppCom
  {
    public int IdAplicacion { get; set; }

    public string Contrasenia { get; set; }

    public int IdAplicacionCom { get; set; }

    public string TokenValue { get; set; }

    public string TimeStamp { get; set; }

    public string SesionHash { get; set; }
  }
}
