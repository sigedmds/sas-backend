﻿namespace AppComunicacion
{
  internal class VistaPreviaDocumentacion
  {
    public string Detalle_Resultado { get; set; }

    public string Codigo_Resultado { get; set; }

    public int Id_Documento { get; set; }

    public byte[] Preview { get; set; }

    public string Extension { get; set; }
  }
}
