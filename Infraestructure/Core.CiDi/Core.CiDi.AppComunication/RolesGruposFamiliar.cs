﻿namespace AppComunicacion
{
  public enum RolesGruposFamiliar
  {
    MODIFICAR_DOMI_GRUPOS = 4,
    MODIFICAR_GRUPOS_APP_INTERNA = 5,
    CONSULTAR_GRUPOS = 9,
  }
}
