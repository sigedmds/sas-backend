﻿using System.Collections.Generic;

namespace AppComunicacion
{
  internal class RespuestaDocList : Respuesta
  {
    public List<Documentacion> Documentos { get; set; }

    public RespuestaDocList()
    {
      this.Documentos = new List<Documentacion>();
    }
  }
}
