﻿using System.Collections.Generic;

namespace AppComunicacion
{
  internal class RespuestaTipoDoc : Respuesta
  {
    public List<TipoDocumentacion> TipoDocList { get; set; }

    public RespuestaTipoDoc()
    {
      this.TipoDocList = new List<TipoDocumentacion>();
    }
  }
}
