﻿using System.Collections.Generic;

namespace AppComunicacion
{
  internal class RespuestaVistaPreviaDoc : Respuesta
  {
    public List<VistaPreviaDocumentacion> Lista_Vista_Previa { get; set; }

    public RespuestaVistaPreviaDoc()
    {
      this.Lista_Vista_Previa = new List<VistaPreviaDocumentacion>();
    }
  }
}
