﻿namespace Core.CiDi.APIs.Model
{
    public class DatosPersonaCompleto
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string IdSexo { get; set; }
        public string NombreSexo { get; set; }
        public string EstadoCivil { get; set; }
        public string IdPais { get; set; }
        public string Pais { get; set; }
        public string Documento { get; set; }
        public string FechaNacimiento { get; set; }
        public int IdNumero { get; set; }
        public int? IdVinDomicilio { get; set; }
        public string DomicilioGrupoFamiliar { get; set; }
        public string DomicilioGrupoFamiliarLocalidad { get; set; }
        public string DomicilioGrupoFamiliarLocalidadId { get; set; }
        public string DomicilioGrupoFamiliarDepartamento { get; set; }
        public string DomicilioGrupoFamiliarProvincia { get; set; }
        public string DomicilioLegal { get; set; }
        public string DomicilioLegalLocalidad { get; set; }
        public string DomicilioLegalProvincia { get; set; }
        public string DomicilioReal { get; set; }
        public string DomicilioRealLocalidad { get; set; }
        public string DomicilioRealProvincia { get; set; }
        public string Cuil { get; set; }
    }
}
