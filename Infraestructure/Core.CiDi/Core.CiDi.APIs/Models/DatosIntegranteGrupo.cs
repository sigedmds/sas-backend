﻿namespace Core.CiDi.APIs.Model
{
    public class DatosIntegranteGrupo
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Documento { get; set; }
        public string IdSexo { get; set; }
        public string NombreSexo { get; set; }
        public double Ingresos { get; set; }
    }
}