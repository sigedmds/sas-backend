﻿namespace Core.CiDi.APIs.Model
{
    public class ResultadoEmail
    {
        public string Resultado { get; set; }
        public string Mensaje { get; set; }
        public string Email { get; set; }
    }
}
