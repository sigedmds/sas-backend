﻿using System.Collections.Generic;

namespace Core.CiDi.APIs.Model
{
    public class DatosGrupoFamiliar
    {
        public decimal IdGrupo { get; set; }
        public List<DatosIntegranteGrupo> Integrantes { get; set; }
    }
}
