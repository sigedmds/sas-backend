﻿namespace Core.CiDi.APIs.Configuration
{
    public enum UrlCidiEnum
    {
        IniciarSesion,
        CerrarSesion,
        CidiUrlApiCuenta,
        CidiUrlApiComunicacion,
        CidiUrlApiDocumentacion,
        CidiUrlApiMobile,
        CidiUrlRelacion,
        CidiUrlDocumentosCdd
    }
}
