﻿namespace Core.CiDi.APIs.Configuration.Sections
{
    public class CiDiEnpoindsSettings
    {
        public string IniciarSesion { get; set; }
        public string CerrarSesion { get; set; }
        public string CidiUrlApiCuenta { get; set; }
        public string CidiUrlApiComunicacion { get; set; }
        public string CidiUrlApiMobile { get; set; }
        public string CidiUrlRelacion { get; set; }
        public string CidiUrlApiDocumentacion { get; set; }
        public string CidiUrlDocumentosCdd { get; set; }
    }
}
