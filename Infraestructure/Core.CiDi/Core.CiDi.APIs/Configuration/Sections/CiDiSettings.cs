﻿namespace Core.CiDi.APIs.Configuration.Sections
{
    public class CiDiSettings
    {
        public string ClientSecret { get; set; }
        public string ClientKey { get; set; }
        public int IdApplication { get; set; }
    }
}
