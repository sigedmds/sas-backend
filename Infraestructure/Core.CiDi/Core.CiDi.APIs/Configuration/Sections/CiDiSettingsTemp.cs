﻿namespace Core.CiDi.APIs.Configuration.Sections
{
    public class CiDiSettingsTemp
    {
        public string ClientSecret { get; set; }
        public string ClientKey { get; set; }
        public int IdApplication { get; set; }
    }
}
