﻿namespace Core.CiDi.APIs.Util
{
    public class TokenSession
    {
        public decimal Iat { get; set; }

        public decimal Exp { get; set; }

        public UserToken Usuario { get; set; }

        public class UserToken
        {
            public decimal Id { get; set; }
            public string Nombre { get; set; }
            public string Apellido { get; set; }
            public string Cuil { get; set; }
            public decimal IdPerfil { get; set; }
            public string Perfil { get; set; }
            public decimal IdNumero { get; set; }
            public string Documento { get; set; }
            public string IdSexo { get; set; }
            public string IdPais { get; set; }
        }
    }

    /*
     * "usuario":{
      "nombre":"NINFA MILAGROS",
      "apellido":"ZEA CARDENAS ",
      "id":7,
      "cuil":"27943383141",
      "idPerfil":1,
      "perfil":"Administrador",
      "idSexo":"02",
      "dni":"94338314",
      "idNumero":0
   },
     */
}
