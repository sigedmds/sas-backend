﻿using Core.Common.ErrorsAndExceptions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

namespace Core.CiDi.APIs.Util
{
    public class UserTokenService
    {

        public TokenSession.UserToken GetSession(IHeaderDictionary headers)
        {
            var stream = headers["authorization"].ToString();
            if (string.IsNullOrEmpty(stream))
                throw new AuthorizationException();
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(stream);
            var tokenS = handler.ReadToken(stream) as JwtSecurityToken;
            var usuario = tokenS.Claims.First(claim => claim.Type == "usuario").Value;
            var t = JsonConvert.DeserializeObject<TokenSession.UserToken>(usuario);
            return t;
        }
    }
}
