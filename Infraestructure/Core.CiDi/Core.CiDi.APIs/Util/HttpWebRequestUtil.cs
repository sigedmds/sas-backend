﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;

namespace Core.CiDi.APIs.Util
{
    public static class HttpWebRequestUtil
    {
        /// <summary>
        /// Realiza la llamada a la WebAPI de Ciudadano Digital, serializa la Entrada y deserializa la Respuesta.
        /// </summary>
        /// <typeparam name="TEntrada">Declarar el objeto de Entrada al método.</typeparam>
        /// <typeparam name="TRespuesta">Declarar el objeto de Respuesta al método.</typeparam>
        /// <param name="accion">Recibe la acción específica del controlador de la WebAPI.</param>
        /// <param name="tEntrada">Objeto de entrada de la WebAPI , especificado en TEntrada.</param>
        /// <returns>Objeto de salida de la WebAPI, especificado en TRespuesta.</returns>
        public static TRespuesta LlamarWebApi<TEntrada, TRespuesta>(string accion, TEntrada tEntrada)
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(accion);
                httpWebRequest.ContentType = "application/json; charset=utf-8";

                String rawjson = JsonConvert.SerializeObject(tEntrada);
                httpWebRequest.Method = "POST";
                var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream());

                streamWriter.Write(rawjson);
                streamWriter.Flush();
                streamWriter.Close();
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var streamReader = new StreamReader(httpResponse.GetResponseStream());
                var result = streamReader.ReadToEnd();
                TRespuesta respuesta = JsonConvert.DeserializeObject<TRespuesta>(result);
                return respuesta;
            }
            catch (WebException ex)
            {
                try
                {
                    var httpResponse = (HttpWebResponse)ex.Response;
                    var streamReader = new StreamReader(httpResponse.GetResponseStream());
                    var result = streamReader.ReadToEnd();
                    TRespuesta respuesta = JsonConvert.DeserializeObject<TRespuesta>(result);
                    return respuesta;
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
