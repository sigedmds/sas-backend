﻿using Core.CiDi.APIs.APIs;
using Core.CiDi.APIs.Configuration;
using Core.CiDi.APIs.Exceptions;
using Core.CiDi.APIs.Model;
using Core.Common.ErrorsAndExceptions;
using Microsoft.AspNetCore.Http;
using System;

namespace Core.CiDi.APIs.Util
{
    public class CiDiUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ApiCuenta _apiCuenta;
        private readonly UserTokenService _userTokenService;

        public CiDiUserService(IHttpContextAccessor httpContextAccessor,
            ApiCuenta apiCuenta)
        {
            _httpContextAccessor = httpContextAccessor;
            _apiCuenta = apiCuenta;
            _userTokenService = new UserTokenService();
        }

        /// <summary>
        /// Obtiene cookie de CiDi
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        public string GetCookieCiDi()
        {
            return _httpContextAccessor.HttpContext?.Request?.Cookies["CiDi"];
        }

        /// <summary>
        /// Obtiene cookie de CiDi
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        public TokenSession.UserToken GetUserToken()
        {
            return _userTokenService.GetSession(_httpContextAccessor.HttpContext?.Request?.Headers);
        }

        /// <summary>
        /// Obtiene información del usuario que se encuentra logueado en la CiDi
        /// </summary>
        /// <param name="cookie"></param>
        /// <returns></returns>
        public UsuarioCidi GetUserLogged()
        {
            try
            {
                var usuario = _apiCuenta.ObtenerUsuarioActivo(GetCookieCiDi());
                if (usuario != null && string.IsNullOrEmpty(usuario.CUIL) && usuario.Respuesta?.Resultado != UrlsCiDi.CiDiOk)
                    throw new APIComunicationException($"Problemas con cidi service: {usuario?.Respuesta.ToString()}");
                usuario.PaiCodPais = GetUserToken().IdPais;
                return usuario;
            }
            catch (APIComunicationException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
