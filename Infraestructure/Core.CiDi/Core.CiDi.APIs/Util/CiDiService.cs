﻿using AppComunicacion;
using AppComunicacion.ApiModels;
using Core.CiDi.APIs.Configuration.Sections;
using Core.CiDi.APIs.Exceptions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;

namespace Core.CiDi.APIs.Util
{
    public class CiDiService
    {
        private readonly IHostingEnvironment _env;
        private readonly CiDiSettings _cidiSettings;
        private readonly CiDiSettingsTemp _cidiSettingsTemp;

        public CiDiService(IHostingEnvironment env,
                           IOptions<CiDiSettings> cidiSettings,
                           IOptions<CiDiSettingsTemp> cidiSettingsTemp)
        {
            _env = env;
            _cidiSettings = cidiSettings.Value;
            _cidiSettingsTemp = cidiSettingsTemp.Value;
        }

        public ServicioComunicacion GetServicio()
        {   /*IMPORTANTE: DESPUES CAMBIAR POR _cidiSettings*/
            var config = new Configuracion
            {
                AppId = _cidiSettings.IdApplication.ToString(),
                AppKey = _cidiSettings.ClientKey,
                AppPass = _cidiSettings.ClientSecret,
                Entorno = _env.IsProduction() ? Entornos.PRODUCCION : Entornos.DESARROLLO
            };
            return new ServicioComunicacion(config);
        }

        public static string GenerarEntidadConsulta(string idSexo, string nroDocumento)
        {
            if (string.IsNullOrEmpty(idSexo) || string.IsNullOrEmpty(nroDocumento))
                return string.Empty;
            return idSexo + nroDocumento.PadLeft(8, '0');
        }

        public static void ValidarCadena(string sexoYDniConcatenado)
        {
            if (sexoYDniConcatenado.Length != 10)
                throw new APIComunicationException("Para consular la api de grupo familiar se espera un cadena de 10 caracteres (dos para sexo y ocho para dni).");
            if (!sexoYDniConcatenado.Substring(0, 2).Equals("01")
                && !sexoYDniConcatenado.Substring(0, 2).Equals("02"))
                throw new APIComunicationException("Para consular la api de grupo familiar el sexo enviado debe ser 01 o 02.");
        }

        private static void ValidarPersonaFiltro(PersonaFiltro personaFiltro)
        {
            if (!personaFiltro.IsValid())
                throw new APIComunicationException("Para consular la api de grupo familiar el sexo enviado debe ser 01 o 02 y el pais enviado debe tener 3 caracteres.");
        }

        public static PersonaFiltro GenerarPersonaFiltro(string sexo, string dni, string pais)
        {
            var personaFiltro = new PersonaFiltro() { Sexo = sexo, PaisTD = pais, NroDocumento = dni };
            ValidarPersonaFiltro(personaFiltro);
            return personaFiltro;
        }
    }
}
