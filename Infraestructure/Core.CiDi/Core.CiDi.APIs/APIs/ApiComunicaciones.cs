﻿using Core.CiDi.APIs.Configuration;
using Core.CiDi.APIs.Configuration.Sections;
using Core.CiDi.APIs.Model;
using Core.CiDi.APIs.Util;
using Microsoft.Extensions.Options;
using System;

namespace Core.CiDi.APIs.APIs
{
    public class ApiComunicaciones
    {
        private static CiDiSettings _cidiSettings;
        private UrlsCiDi _globalVars;

        public ApiComunicaciones(IOptions<CiDiSettings> cidiSettings,
                    IOptions<CiDiEnpoindsSettings> cidiEnpoindsSettings)
        {
            _cidiSettings = cidiSettings.Value;
            _globalVars = new UrlsCiDi(cidiSettings, cidiEnpoindsSettings);
        }


        public ResultadoEmail EnviarMailPorCuil(DatosEmail datosEmail)
        {
            var email = new Email
            {
                Cuil = datosEmail.Cuil,
                Asunto = datosEmail.Asunto,
                Subtitulo = string.Empty,
                Mensaje = datosEmail.Asunto,
                InfoDesc = datosEmail.InfoDesc,
                InfoDato = datosEmail.InfoDato,
                InfoLink = datosEmail.InfoLink,
                Firma = datosEmail.Firma,
                Ente = datosEmail.Ente,
                Id_App = _cidiSettings.IdApplication,
                Pass_App = _cidiSettings.ClientSecret,
                TimeStamp = DateTime.Now.ToString("yyyyMMddHHmmssfff")
            };
            email.TokenValue = TokenUtil.ObtenerToken_SHA1(email.TimeStamp, _cidiSettings.ClientKey);

            return HttpWebRequestUtil.LlamarWebApi<Email, ResultadoEmail>(UrlsCiDi.ApiComunicacion.Email.Enviar, email);
        }
    }
}
