﻿using Core.CiDi.APIs.Configuration;
using Core.CiDi.APIs.Configuration.Sections;
using Core.CiDi.APIs.Model;
using Core.CiDi.APIs.Util;
using Microsoft.Extensions.Options;
using System;

namespace Core.CiDi.APIs.APIs
{
    public class ApiCuenta
    {
        private static CiDiSettings _cidiSettings;
        private UrlsCiDi _globalVars;

        public ApiCuenta(IOptions<CiDiSettings> cidiSettings,
                    IOptions<CiDiEnpoindsSettings> cidiEnpoindsSettings)
        {
            _cidiSettings = cidiSettings.Value;
            _globalVars = new UrlsCiDi(cidiSettings, cidiEnpoindsSettings);
        }

        public UsuarioCidi ObtenerUsuarioActivo(string cookieHash)
        {
            return ObtenerUsuario(cookieHash, null);
        }

        public UsuarioCidi ObtenerUsuarioPorCuil(string cookieHash, string cuil)
        {
            return (string.IsNullOrEmpty(cuil)) ? null : ObtenerUsuario(cookieHash, cuil);
        }

        public bool EsUsuarioNivelDos(string cookieHash, string cuil)
        {
            return (!string.IsNullOrEmpty(cuil)) && EsUsuarioNivelDos(ObtenerUsuario(cookieHash, null));
        }

        public bool EsUsuarioNivelDos(string cookieHash)
        {
            return EsUsuarioNivelDos(ObtenerUsuario(cookieHash, null));
        }

        public bool EsUsuarioNivelDos(UsuarioCidi usuario)
        {
            return usuario.Id_Estado.HasValue && usuario.Id_Estado.Value == 2;
        }

        private UsuarioCidi ObtenerUsuario(string cookieHash, string cuil)
        {

            var entrada = new Entrada
            {
                IdAplicacion = _cidiSettings.IdApplication,
                Contrasenia = _cidiSettings.ClientSecret,
                HashCookie = cookieHash,
                TimeStamp = DateTime.Now.ToString("yyyyMMddHHmmssfff")
            };

            if (!string.IsNullOrEmpty(cuil))
                entrada.CUIL = cuil;

            entrada.TokenValue = TokenUtil.ObtenerToken_SHA1(entrada.TimeStamp, _cidiSettings.ClientKey);
            var optenerCuentaUrl = string.IsNullOrEmpty(cuil)
                ? UrlsCiDi.ApiCuenta.Usuario.ObtenerUsuarioAplicacion
                : UrlsCiDi.ApiCuenta.Usuario.ObtenerUsuario;
            return HttpWebRequestUtil.LlamarWebApi<Entrada, UsuarioCidi>(optenerCuentaUrl, entrada);
        }
    }
}
