﻿using AppComunicacion;
using AppComunicacion.ApiModels;
using Core.CiDi.APIs.Configuration;
using Core.CiDi.APIs.Configuration.Sections;
using Core.CiDi.APIs.Util;
using Microsoft.Extensions.Options;
using System;

namespace Core.CiDi.APIs.APIs
{
    public class ApiDomicilios
    {
        private readonly CiDiService _ciDiService;
        private UrlsCiDi _globalVars;

        public ApiDomicilios(IOptions<CiDiSettings> cidiSettings,
                    IOptions<CiDiEnpoindsSettings> cidiEnpoindsSettings,
                    CiDiService ciDiService)
        {
            _ciDiService = ciDiService;
            _globalVars = new UrlsCiDi(cidiSettings, cidiEnpoindsSettings);
        }


        #region Apis Domicilio

        public Domicilio ApiConsultaDatosBasicos(string cookieHash, string sexoYDniConcatenado)
        {
            return Model(cookieHash, sexoYDniConcatenado, RolesAPIDomicilios.CONSULTAR_DATOS_BASICOS);
        }

        public Domicilio ApiConsultaConCaracteristicas(string cookieHash, string sexoYDniConcatenado)
        {
            return Model(cookieHash, sexoYDniConcatenado, RolesAPIDomicilios.CONSULTAR_CON_CARACTERISTICAS);
        }

        public Domicilio ApiConsultaDomicilioGenerado(string cookieHash, string entidad,
            int tipoDomicilio, int appId, string cuilUsuario)
        {
            return Model(cookieHash, entidad, tipoDomicilio, cuilUsuario, appId,
                RolesAPIDomicilios.CONSULTAR_DOMICILIO_GEN);
        }

        public string ApiConsultaDatosBasicosJson(string cookieHash, string sexoYDniConcatenado)
        {
            return ModelJson(cookieHash, sexoYDniConcatenado, RolesAPIDomicilios.CONSULTAR_DATOS_BASICOS);
        }

        public string ApiConsultaConCaracteristicasJson(string cookieHash, string sexoYDniConcatenado)
        {
            return ModelJson(cookieHash, sexoYDniConcatenado, RolesAPIDomicilios.CONSULTAR_CON_CARACTERISTICAS);
        }

        private Domicilio Model(string cookieHash, string sexoYDniConcatenado, RolesAPIDomicilios rol)
        {
            CiDiService.ValidarCadena(sexoYDniConcatenado);
            return _ciDiService.GetServicio().ApiDomicilios(cookieHash, sexoYDniConcatenado, rol);
        }

        private Domicilio Model(string cookieHash, string entidad, int tipoDomicilio,
            string cuilUsuario, int appId, RolesAPIDomicilios rol)
        {
            try
            {
                return _ciDiService.GetServicio().ApiDomicilios(cookieHash,
                    entidad, rol, appId, tipoDomicilio, cuilUsuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string ModelJson(string cookieHash, string sexoYDniConcatenado, RolesAPIDomicilios rol)
        {
            CiDiService.ValidarCadena(sexoYDniConcatenado);
            return _ciDiService.GetServicio().ApiDomiciliosJSON(cookieHash, sexoYDniConcatenado, rol);
        }

        #endregion


        #region Apis Caracteristicas Domicilio

        public CaracteristicasDomicilio ApiConsultaCaracteristicasDomicilio(string cookieHash,
            string sexoYDniConcatenado)
        {
            return Model(cookieHash, sexoYDniConcatenado, RolesAPICaracteristicasDomicilio.CONSULTAR);
        }

        public string ApiConsultaCaracteristicasDomicilioJson(string cookieHash, string sexoYDniConcatenado)
        {
            return ModelJson(cookieHash, sexoYDniConcatenado, RolesAPICaracteristicasDomicilio.CONSULTAR);
        }

        private CaracteristicasDomicilio Model(string cookieHash, string sexoYDniConcatenado,
            RolesAPICaracteristicasDomicilio rol)
        {
            CiDiService.ValidarCadena(sexoYDniConcatenado);
            return _ciDiService.GetServicio().ApiCaracteristicasDomicilio(cookieHash, sexoYDniConcatenado, rol);
        }

        private string ModelJson(string cookieHash, string sexoYDniConcatenado,
            RolesAPICaracteristicasDomicilio rol)
        {
            CiDiService.ValidarCadena(sexoYDniConcatenado);
            return _ciDiService.GetServicio()
                .ApiCaracteristicasDomicilioJSON(cookieHash, sexoYDniConcatenado, rol);
        }

        #endregion


        #region Urls

        public string UrlConsultar(string cuilUsuarioLogueado, string sexoYDniConcatenado)
        {
            return Url(cuilUsuarioLogueado, sexoYDniConcatenado, RolesDomicilio.CONSULTAR_DOMICILIO);
        }

        public string UrlModificarCaracteristicas(string cuilUsuarioLogueado, string idVin)
        {
            return Url(cuilUsuarioLogueado, idVin, RolesDomicilio.MODIFICAR_CARACTERISTICAS);
        }

        public string UrlAlta(string cuilUsuarioLogueado, string entidad)
        {
            var tipoDomicilio = 3;
            return Url(cuilUsuarioLogueado, entidad, RolesDomicilio.ALTA_DOMICILIO_GENERICO, tipoDomicilio, JurisdiccionDomicilio.PROVINCIAL);
        }

        public string UrlConsultarCaracteristicas(string cuilUsuarioLogueado, string sexoYDniConcatenado)
        {
            return Url(cuilUsuarioLogueado, sexoYDniConcatenado, RolesDomicilio.CONSULTAR_CARACTERISTICAS);
        }

        private string Url(string cuilUsuarioLogueado, string sexoYDniConcatenado, RolesDomicilio rol)
        {
            if (!rol.Equals(RolesDomicilio.MODIFICAR_CARACTERISTICAS))
            {
                CiDiService.ValidarCadena(sexoYDniConcatenado);
            }

            return _ciDiService.GetServicio().Domicilios(cuilUsuarioLogueado, sexoYDniConcatenado, rol);
        }

        private string Url(string cuilUsuarioLogueado, string entidad, RolesDomicilio rol, int tipoDomicilio, JurisdiccionDomicilio jurisdiccion)
        {
            if (!rol.Equals(RolesDomicilio.MODIFICAR_CARACTERISTICAS) && !rol.Equals(RolesDomicilio.ALTA_DOMICILIO_GENERICO))
            {
                CiDiService.ValidarCadena(entidad);
            }

            return _ciDiService.GetServicio().Domicilios(cuilUsuarioLogueado, entidad, rol, tipoDomicilio, jurisdiccion);
        }

        #endregion

    }
}