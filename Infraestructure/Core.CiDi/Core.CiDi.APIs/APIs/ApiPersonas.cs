﻿using AppComunicacion;
using AppComunicacion.ApiModels;
using Core.CiDi.APIs.Configuration;
using Core.CiDi.APIs.Configuration.Sections;
using Core.CiDi.APIs.Util;
using Microsoft.Extensions.Options;
using System.Collections.Generic;

namespace Core.CiDi.APIs.APIs
{
    public class ApiPersonas
    {

        private readonly CiDiService _ciDiService;
        private UrlsCiDi _globalVars;

        public ApiPersonas(IOptions<CiDiSettings> cidiSettings,
                    IOptions<CiDiEnpoindsSettings> cidiEnpoindsSettings,
                    CiDiService ciDiService)
        {
            _ciDiService = ciDiService;
            _globalVars = new UrlsCiDi(cidiSettings, cidiEnpoindsSettings);
        }

        #region Apis Persona

        public PersonaUnica ApiConsultaDatosBasicos(string cookieHash, string sexo, string dni, string pais)
        {            
            return Model(cookieHash, sexo, dni, pais, RolesAPIPersonas.CONSULTAR_DATOS_BASICOS);
        }
        
        public PersonaUnica ApiConsultaDatosCompleto(string cookieHash, string sexo, string dni, string pais)
        {
            return Model(cookieHash, sexo, dni, pais, RolesAPIPersonas.CONSULTAR_DATOS_COMPLETO);
        }

        public PersonaUnica ApiConsultaDatosConCaracteristicas(string cookieHash, string sexo, string dni, string pais)
        {
            return Model(cookieHash, sexo, dni, pais, RolesAPIPersonas.CONSULTAR_DATOS_CON_CARACTERISTICAS);
        }

        public PersonaUnica ApiConsultaDatosConDomicilios(string cookieHash, string sexo, string dni, string pais)
        {
            return Model(cookieHash, sexo, dni, pais, RolesAPIPersonas.CONSULTAR_DATOS_CON_DOMICILIOS);
        }

        public string ApiConsultaDatosBasicosJson(string cookieHash, string sexo, string dni, string pais)
        {
            return ModelJson(cookieHash, sexo, dni, pais, RolesAPIPersonas.CONSULTAR_DATOS_BASICOS);
        }

        public string ApiConsultaDatosCompletoJson(string cookieHash, string sexo, string dni, string pais)
        {
            return ModelJson(cookieHash, sexo, dni, pais, RolesAPIPersonas.CONSULTAR_DATOS_COMPLETO);
        }

        public string ApiConsultaDatosConCaracteristicasJson(string cookieHash, string sexo, string dni, string pais)
        {
            return ModelJson(cookieHash, sexo, dni, pais, RolesAPIPersonas.CONSULTAR_DATOS_CON_CARACTERISTICAS);
        }

        public string ApiConsultaDatosConDomiciliosJson(string cookieHash, string sexo, string dni, string pais)
        {
            return ModelJson(cookieHash, sexo, dni, pais, RolesAPIPersonas.CONSULTAR_DATOS_CON_DOMICILIOS);
        }

        private PersonaUnica Model(string cookieHash, string sexo, string dni, string pais, RolesAPIPersonas rol)
        {
            return _ciDiService.GetServicio().ApiPersonas(cookieHash, CiDiService.GenerarPersonaFiltro(sexo, dni, pais), rol);
        }

        private string ModelJson(string cookieHash, string sexo, string dni, string pais, RolesAPIPersonas rol)
        {
            return _ciDiService.GetServicio().ApiPersonasJSON(cookieHash, CiDiService.GenerarPersonaFiltro(sexo, dni, pais), rol);
        }

        #endregion

        #region Apis Caracteristicas Persona

        public List<Caracteristica> ApiConsultaCaracteristicasPersona(string cookieHash, string sexo, string dni, string pais)
        {
            return Model(cookieHash, sexo, dni, pais, RolesAPICaracteristicasPersona.CONSULTAR);
        }
      
        public string ApiConsultaCaracteristicasPersonaJson(string cookieHash, string sexo, string dni, string pais)
        {
            return ModelJson(cookieHash, sexo, dni, pais, RolesAPICaracteristicasPersona.CONSULTAR);
        }

        private List<Caracteristica> Model(string cookieHash, string sexo, string dni, string pais, RolesAPICaracteristicasPersona rol)
        {
            return _ciDiService.GetServicio().ApiCaracteristicasPersona(cookieHash, CiDiService.GenerarPersonaFiltro(sexo, dni, pais), rol);
        }

        private string ModelJson(string cookieHash, string sexo, string pais, string documento, RolesAPICaracteristicasPersona rol)
        {
            var persona = new PersonaFiltro(){ Sexo = sexo , PaisTD = pais, NroDocumento = documento};
            return _ciDiService.GetServicio().ApiCaracteristicasPersonaJSON(cookieHash, persona, rol);
        }

        #endregion

        #region Urls

        public string UrlAlta(string cuilUsuarioLogueado, string sexo, string dni, string pais)
        {
            return Url(cuilUsuarioLogueado, sexo, dni, pais, RolesPersonas.ALTA_PERSONA);
        }

        public string UrlConsultar(string cuilUsuarioLogueado, string sexo, string dni, string pais)
        {
            return Url(cuilUsuarioLogueado, sexo, dni, pais, RolesPersonas.CONSULTAR_PERSONA_COMPLETO);
        }
        
        public string UrlModificar(string cuilUsuarioLogueado, string sexo, string dni, string pais)
        {
            return Url(cuilUsuarioLogueado, sexo, dni, pais, RolesPersonas.MODIFICAR_PERSONA);
        }

        public string UrlConsultarCaracteristicas(string cuilUsuarioLogueado, string sexo, string dni, string pais)
        {
            return Url(cuilUsuarioLogueado, sexo, dni, pais, RolesPersonas.CONSULTAR_CARACTERISTICAS);
        }

        public string UrlModificarCaracteristicas(string cuilUsuarioLogueado, string sexo, string dni, string pais)
        {
            return Url(cuilUsuarioLogueado, sexo, dni, pais, RolesPersonas.MODIFICAR_CARACTERISTICAS);
        }

        private string Url(string cuilUsuarioLogueado, string sexo, string dni, string pais, RolesPersonas rol)
        {
            return _ciDiService.GetServicio().Personas(cuilUsuarioLogueado, CiDiService.GenerarPersonaFiltro(sexo, dni, pais), rol);
        }

        #endregion
    }
}
