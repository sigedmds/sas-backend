﻿using AppComunicacion;
using AppComunicacion.ApiModels;
using Core.CiDi.APIs.Configuration;
using Core.CiDi.APIs.Configuration.Sections;
using Core.CiDi.APIs.Util;
using Microsoft.Extensions.Options;

namespace Core.CiDi.APIs.APIs
{
    public class ApiGruposFamiliares
    {

        private readonly CiDiService _ciDiService;
        private UrlsCiDi _globalVars;

        public ApiGruposFamiliares(IOptions<CiDiSettings> cidiSettings,
                    IOptions<CiDiEnpoindsSettings> cidiEnpoindsSettings,
                    CiDiService ciDiService)
        {
            _ciDiService = ciDiService;
            _globalVars = new UrlsCiDi(cidiSettings, cidiEnpoindsSettings);
        }


        #region Apis

        public RespuestaAPIGrupoFamiliar ApiConsultaGrupos(string cookieHash, string sexo, string dni, string pais)
        {
            return Model(cookieHash, sexo, dni, pais, RolesAPIGruposFamiliar.CONSULTAR_GRUPOS);
        }

        public RespuestaAPIGrupoFamiliar ApiConsultaGruposConCaractPersonas(string cookieHash, string sexo, string dni, string pais)
        {
            return Model(cookieHash, sexo, dni, pais, RolesAPIGruposFamiliar.CONSULTAR_GRUPOS_CON_CARACT_DE_PERSONAS);
        }

        public string ApiConsultaGruposJson(string cookieHash, string sexo, string dni, string pais)
        {
            return ModelJson(cookieHash, sexo, dni, pais, RolesAPIGruposFamiliar.CONSULTAR_GRUPOS);
        }

        public string ApiConsultaGruposConCaractPersonasJson(string cookieHash, string sexo, string dni, string pais)
        {
            return ModelJson(cookieHash, sexo, dni, pais, RolesAPIGruposFamiliar.CONSULTAR_GRUPOS_CON_CARACT_DE_PERSONAS);
        }

        private RespuestaAPIGrupoFamiliar Model(string cookieHash, string sexo, string dni, string pais, RolesAPIGruposFamiliar rol)
        {
            return _ciDiService.GetServicio().ApiGruposFamiliares(cookieHash, CiDiService.GenerarPersonaFiltro(sexo, dni, pais), rol);
        }

        private string ModelJson(string cookieHash, string sexo, string dni, string pais, RolesAPIGruposFamiliar rol)
        {
            return _ciDiService.GetServicio().ApiGruposFamiliaresJSON(cookieHash, CiDiService.GenerarPersonaFiltro(sexo, dni, pais), rol);
        }

        #endregion

        #region Urls

        public string UrlModificarAplicacionExterna(string cuilUsuarioLogueado, string sexo, string dni, string pais)
        {
            return Url(cuilUsuarioLogueado, sexo, dni, pais, RolesGruposFamiliar.MODIFICAR_GRUPOS_APP_INTERNA);
        }

        public string UrlModificarAplicacionInterna(string cuilUsuarioLogueado, string sexo, string dni, string pais)
        {
            return Url(cuilUsuarioLogueado, sexo, dni, pais, RolesGruposFamiliar.MODIFICAR_GRUPOS_APP_INTERNA);
        }


        private string Url(string cuilUsuarioLogueado, string sexo, string dni, string pais, RolesGruposFamiliar rol)
        {
            return _ciDiService.GetServicio().GruposFamiliares(cuilUsuarioLogueado, CiDiService.GenerarPersonaFiltro(sexo, dni, pais), rol);
        }

        #endregion
    }
}
