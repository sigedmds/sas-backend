﻿using Core.CiDi.APIs.Configuration;
using Core.CiDi.APIs.Configuration.Sections;
using Core.CiDi.APIs.Exceptions;
using Core.CiDi.APIs.Model;
using Core.CiDi.APIs.Util;
using Core.CiDi.Documents.Entities.CDDAutorizador;
using Core.CiDi.Documents.Entities.CDDPost;
using Core.CiDi.Documents.Entities.CDDResponse;
using Core.CiDi.Documents.Utils;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.CiDi.APIs.APIs
{
    public class ApiDocumentos
    {

        private UrlsCiDi _globalVars;

        public ApiDocumentos(IOptions<CiDiSettings> cidiSettings,
                    IOptions<CiDiEnpoindsSettings> cidiEnpoindsSettings)
        {
            _globalVars = new UrlsCiDi(cidiSettings, cidiEnpoindsSettings);
        }

        private CryptoManagerV4._0.Clases.CryptoDiffieHellman ObjDiffieHellman { get; set; }
        private Autorizador ObjAutorizador { get; set; }
        private CDDPost RequestPost { get; set; }
        private DocumentoCdd DocumentoCdd { get; }
        private string CuilUsuarioLogueado { get; }
        private IList<string> FormatosPermitidos { get; }
        private int IdDocumento { get; set; }
        private int IdAplicacionOrigen { get; }
        private string Password { get; }
        private string Key { get; }
        private string Operacion { get; set; }

        private const string OperacionConsultar = "1";
        private const string OperacionAdjuntar = "1";//3??
        private const string OperacionEliminar = "1";

        /// <summary>
        /// Inicializa los datos necesarios para consultar la api de documentos.
        /// </summary>
        /// <param name="documento">Datos del documento a registrar/consultar/eliminar</param>
        /// <param name="cuilUsuarioLogueado">usuario logueado en cidi</param>
        /// <param name="formatosPermitidos">Attay de string de formatos permitidos. Ej ["pdf","jpg"]</param>
        /// <param name="credenciales">Credenciales para autorizar el consumo de la api de documentos de CiDi. Se debe solicitar a gobierno.</param>
        public ApiDocumentos(DocumentoCdd documento, string cuilUsuarioLogueado, IList<string> formatosPermitidos,
            CredencialesAutorizacion credenciales)
        {
            IdAplicacionOrigen = credenciales.IdAppOrigen;
            Password = credenciales.Password;
            Key = credenciales.Key;
            DocumentoCdd = documento;
            CuilUsuarioLogueado = cuilUsuarioLogueado;
            FormatosPermitidos = formatosPermitidos;
            ValidarDatosRequeridos();
        }

        #region public Methods

        public CDDResponseConsulta BuscarDocumentoCdd()
        {
            ValidarDatosRequeridosBusquedaDocumento();

            IdDocumento = DocumentoCdd.IdDocumento;

            Operacion = OperacionConsultar;

            var respLogin = GetAuthorizeWebApiCdd();

            if (respLogin != null && respLogin.Codigo_Resultado.Equals("SEO"))
            {
                SetParametersRequestPost(respLogin.Llave_BLOB_Login);

                var response = GetDocumentWebApiCdd();

                if (response != null && response.Codigo_Resultado.Equals("SEO"))
                    return response;
                if (response != null)
                    throw new APIComunicationException("Codigo Error: " + response.Codigo_Resultado + " Descripcion: " +
                                                    response.Detalle_Resultado);
            }
            else
            {
                if (respLogin != null)
                    throw new APIComunicationException("Codigo Error: " + respLogin.Codigo_Resultado + " Descripcion: " +
                                                    respLogin.Detalle_Resultado);
            }

            return null;
        }

        public CDDResponseInsercion GuardarNuevoDocumentoCdd()
        {
            IdDocumento = 0;

            ValidarDatosRequeridosNuevoDocumento();

            Operacion = OperacionAdjuntar;

            var respLogin = GetAuthorizeWebApiCdd();

            if (respLogin != null && respLogin.Codigo_Resultado.Equals("SEO"))
            {
                SetParametersRequestPost(respLogin.Llave_BLOB_Login);

                var response = SaveDocumentWebApiCdd();

                if (response != null && response.Codigo_Resultado.Equals("SEO"))
                    return response;

                if (response != null)
                    throw new APIComunicationException("Codigo Error: " + response.Codigo_Resultado + " Descripcion: " +
                                                    response.Detalle_Resultado);
            }
            else
            {
                if (respLogin != null)
                    throw new APIComunicationException("Codigo Error: " + respLogin.Codigo_Resultado + " Descripcion: " +
                                                    respLogin.Detalle_Resultado);
            }

            return null;
        }

        public CDDResponse EliminarDocumentoCdd()
        {
            ValidarDatosRequeridosEliminacionDocumento();

            Operacion = OperacionEliminar;

            var respLogin = GetAuthorizeWebApiCdd();

            if (respLogin != null && respLogin.Codigo_Resultado.Equals("SEO"))
            {
                var respCdd = SetearDataPostEliminar(respLogin.Llave_BLOB_Login);

                var cddRespEliminacion = EliminarDocumentWebApiCdd(respCdd);

                if (cddRespEliminacion != null && cddRespEliminacion.Codigo_Resultado.Equals("SEO"))

                    return cddRespEliminacion;

                if (cddRespEliminacion != null)
                    throw new APIComunicationException("Codigo Error: " + cddRespEliminacion.Codigo_Resultado + " Descripcion: " +
                                                    cddRespEliminacion.Detalle_Resultado);
            }
            else
            {
                if (respLogin != null)
                    throw new APIComunicationException("Codigo Error: " + respLogin.Codigo_Resultado + " Descripcion: " +
                                                    respLogin.Detalle_Resultado);
            }

            return null;
        }

        #endregion

        #region private Methods

        private void ValidarDatosRequeridos()
        {
            if (IdAplicacionOrigen == 0)
                throw new APIComunicationException("El id de aplicación origen es requerido.");
            if (Password == null)
                throw new APIComunicationException("La password es requerida.");
            if (Key == null)
                throw new APIComunicationException("La key es requerida.");
            if (DocumentoCdd == null)
                throw new APIComunicationException("El documento es requerido");
            if (string.IsNullOrEmpty(CuilUsuarioLogueado))
                throw new APIComunicationException("El cuil del usuario logueado es requerido.");
            if (DocumentoCdd.IdCatalogo == 0)
                throw new APIComunicationException("El tipo de documento (id de catálogo) es requerido.");
        }

        private void ValidarDatosRequeridosNuevoDocumento()
        {
            if (DocumentoCdd.BlobImagen == null)
                throw new APIComunicationException("El contenido del documento es requerido.");
            if (FormatosPermitidos == null)
                throw new APIComunicationException("La lista de formatos permitidos es requerida.");
            if (FormatosPermitidos.Count == 0)
                throw new APIComunicationException("La lista de formatos permitidos debe poseer como mínimo un formato.");
            if (string.IsNullOrEmpty(DocumentoCdd.NombreDocumento))
                throw new APIComunicationException("El nombre del documento es requerido.");
            if (string.IsNullOrEmpty(DocumentoCdd.Extension))
                throw new APIComunicationException("La extensión del documento es requerida.");
        }

        private void ValidarDatosRequeridosBusquedaDocumento()
        {
            if (DocumentoCdd.IdDocumento == 0)
                throw new APIComunicationException("El id del documento es requerido.");
        }

        private void ValidarDatosRequeridosEliminacionDocumento()
        {
            if (DocumentoCdd.IdDocumento == 0)
                throw new APIComunicationException("El id del documento es requerido.");
        }

        private void SetParametersRequestPost(byte[] pBlobKey)
        {
            RequestPost = new CDDPost
            {
                Id_Aplicacion_Origen = ObjAutorizador.Id_Aplicacion_Origen,
                Pwd_Aplicacion = ObjAutorizador.Pwd_Aplicacion,
                IdUsuario = ObjAutorizador.Id_Usuario,
                Shared_Key = pBlobKey,
                Id_Documento = IdDocumento,
                Id_Catalogo = DocumentoCdd.IdCatalogo
            };

            if (IdDocumento == 0)
                AdjuntarDocumento();
        }

        private CDDPostExpediente SetearDataPostEliminar(byte[] llaveBlobLogin)
        {
            var cddPostExpediente = new CDDPostExpediente
            {
                Id_Aplicacion_Origen = IdAplicacionOrigen,
                Pwd_Aplicacion = Password,
                Shared_Key = null,
                IdUsuario = CuilUsuarioLogueado,
                N_Documento = string.Empty,
                Vigencia = null,
                Blob_Imagen = null,
                Extension = string.Empty,
                Id_Catalogo = DocumentoCdd.IdCatalogo
            };
            cddPostExpediente.Vigencia = null;
            cddPostExpediente.Id_Documentacion = string.Empty;
            cddPostExpediente.Id_Documento = DocumentoCdd.IdDocumento;
            cddPostExpediente.Id_Tramite = string.Empty;
            cddPostExpediente.Id_Expediente = string.Empty;
            cddPostExpediente.Id_Pase = 0;
            cddPostExpediente.Id_Cuerpo = 0;
            cddPostExpediente.Id_Foja = 0;
            cddPostExpediente.N_Usuario = CuilUsuarioLogueado; // Colocar el Cuil del Operador
            cddPostExpediente.N_Descripcion = string.Empty;
            cddPostExpediente.Documentacion.Descripcion = string.Empty;
            cddPostExpediente.Shared_Key = llaveBlobLogin;
            return cddPostExpediente;
        }

        private void AdjuntarDocumento()
        {
            var objCryptoHash = new CryptoManagerV4._0.Clases.CryptoHash();
            string mensaje;
            ValidarFormatosUploadValidos();
            RequestPost.N_Documento = DocumentoCdd.NombreDocumento;
            RequestPost.Extension = DocumentoCdd.Extension;
            RequestPost.Blob_Imagen = objCryptoHash.Cifrar_Datos(DocumentoCdd.BlobImagen, out mensaje);
            RequestPost.Vigencia = DocumentoCdd.Vigencia;
            RequestPost.N_Constatado = true;
            RequestPost.N_Descripcion = DocumentoCdd.Descripcion;
        }

        private CDDResponseInsercion SaveDocumentWebApiCdd()
        {
            return HttpWebRequestUtil.LlamarWebApi<CDDPost, CDDResponseInsercion>(UrlsCiDi.ApiCdd.Documentos.GuardarDocumento, RequestPost);
        }

        private CDDResponseEliminacion EliminarDocumentWebApiCdd(CDDPostExpediente pCddPostExpediente)
        {
            return HttpWebRequestUtil.LlamarWebApi<CDDPostExpediente, CDDResponseEliminacion>(UrlsCiDi.ApiCdd.Documentos.EliminarDocumento, pCddPostExpediente);
        }

        private void ValidarFormatosUploadValidos()
        {
            if (FormatosPermitidos.Any(formatoPermitido => DocumentoCdd.Extension.ToUpper().Equals(formatoPermitido.ToUpper())))
                throw new APIComunicationException("Formato no permitido para el documento que se intenta almacenar.");
        }

        private CDDResponseLogin GetAuthorizeWebApiCdd()
        {
            InitializeAuthorize();

            var cddRespLogin = HttpWebRequestUtil.LlamarWebApi<Autorizador, CDDResponseLogin>(UrlsCiDi.ApiCdd.Documentos.AutorizarSolicitud, ObjAutorizador);

            if (cddRespLogin != null && cddRespLogin.Codigo_Resultado.Equals("SEO"))
                CompleteAuthorize(cddRespLogin.Llave_BLOB_Login);

            return cddRespLogin;
        }

        private CDDResponseConsulta GetDocumentWebApiCdd()
        {
            return HttpWebRequestUtil.LlamarWebApi<CDDPost, CDDResponseConsulta>(UrlsCiDi.ApiCdd.Documentos.ObtenerDocumento, RequestPost);
        }

        private void InitializeAuthorize()
        {
            ObjDiffieHellman = new CryptoManagerV4._0.Clases.CryptoDiffieHellman();
            ObjAutorizador = new Autorizador();

            var helper = new Helper();
            var cmd = new CryptoManagerV4._0.General.CryptoManagerData();

            var mensaje = string.Empty;

            ObjAutorizador.Id_Aplicacion_Origen = IdAplicacionOrigen;
            ObjAutorizador.Pwd_Aplicacion = helper.Encriptar_Password(Password); ;
            ObjAutorizador.Key_Aplicacion = Key;
            ObjAutorizador.Operacion = Operacion; // 1 = Consulta ; 3 = Adjuntar
            ObjAutorizador.Id_Usuario = CuilUsuarioLogueado;
            ObjAutorizador.Time_Stamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");

            try
            {
                ObjAutorizador.Token = cmd.Get_Token(ObjAutorizador.Time_Stamp, ObjAutorizador.Key_Aplicacion);

                if (!string.IsNullOrEmpty(mensaje)) throw new APIComunicationException(mensaje);
            }
            catch (Exception ex)
            {
                throw new APIComunicationException(ex.Message);
            }

            // Creo la llave CNG
            ObjDiffieHellman.Create_Key_ECCDHP521();

            // Creo la llave Blob pública
            ObjAutorizador.Public_Blob_Key = ObjDiffieHellman.Export_Key_Material();

            // Llave compartida
            ObjAutorizador.Shared_Key = null;

        }

        private void CompleteAuthorize(byte[] pPublicBlobKey)
        {
            try
            {
                // Creo la llave compartida
                ObjAutorizador.Shared_Key = ObjDiffieHellman.Share_Key_Generate(pPublicBlobKey);
            }
            catch (Exception ex)
            {
                throw new APIComunicationException(ex.Message);
            }
        }

        #endregion
    }
}
