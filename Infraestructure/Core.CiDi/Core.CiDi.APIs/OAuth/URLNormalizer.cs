﻿using System.Text.RegularExpressions;

namespace Core.CiDi.APIs.OAuth
{
    public class URLNormalizer
    {
        public string Normalizar(string endPointPath)
        {
            Regex r = new Regex(@"\d+", RegexOptions.None);
            endPointPath = r.Replace(endPointPath, ":id");
            return endPointPath;
        }
    }
}
