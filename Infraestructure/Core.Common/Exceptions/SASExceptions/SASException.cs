﻿using System;
using System.Net;

namespace Core.Common.Exceptions.SASExceptions
{
	public class SASException : HttpStatusCodeException
	{
		public SASException(HttpStatusCode statusCode) : base(statusCode)
		{
		}
		public SASException(HttpStatusCode statusCode, string message) : base(statusCode, message)
		{
		}
		public SASException(HttpStatusCode statusCode, Exception innerException) : base(statusCode, innerException)
		{
		}

		public SASException(string message) : base(HttpStatusCode.BadRequest, message)
		{

		}
		public SASException(Exception innerException) : base(HttpStatusCode.BadRequest, innerException)
		{

		}
	}
}
