﻿using System;
using System.Collections.Generic;

namespace Core.Common.ErrorsAndExceptions
{
    public class ErrorException : ApplicationException
    {
        private IList<string> _errores;
        private readonly int _codigo;
        private readonly string _descripcion;
        private readonly string _titulo;

        public int StatusCode
        {
            get => _codigo;
        }

        public string Description
        {
            get => _descripcion;
        }

        public string Titulo
        {
            get => _titulo;
        }

        public IList<string> Errores
        {
            get => _errores;
            set => _errores = value;
        }

        public ErrorException(string titulo, string mensaje, string descripcion, int codigo) :
            base(mensaje)
        {
            _titulo = titulo;
            _codigo = codigo;
            _descripcion = descripcion;
            _errores = new List<string>();
        }

        public void AgregarMensaje(string mensaje)
        {
            foreach (var msj in _errores)
            {
                if (msj.ToLowerInvariant().CompareTo(mensaje.ToLowerInvariant()) == 0)
                    return;
            }
            _errores.Add(mensaje);
        }

        public void AgregarMensajes(IList<string> mensajes)
        {
            foreach (var msj in mensajes)
            {
                AgregarMensaje(msj);
            }
        }

        public bool HayErrores
        {
            get
            {
                return this._errores.Count > 0;
            }
        }
    }
}
