﻿using System.Net;

namespace Core.Common.ErrorsAndExceptions
{
    public class NoValidException : ErrorException
    {


        public NoValidException() :
            base("Revise los siguientes puntos", null, null, (int)HttpStatusCode.BadRequest)
        {
        }

        public NoValidException(string titulo, string mensaje, string descripcion) :
            base(titulo, mensaje, descripcion, (int)HttpStatusCode.BadRequest)
        {
        }

        public NoValidException(string titulo, string mensaje) :
            base(titulo, mensaje, null, (int)HttpStatusCode.BadRequest)
        {
        }

        public NoValidException(string mensaje) :
            base("Revise los siguientes puntos", mensaje, null, (int)HttpStatusCode.BadRequest)
        {
            Errores.Add(mensaje);
        }

    }
}
