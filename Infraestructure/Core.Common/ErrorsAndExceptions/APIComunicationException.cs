﻿using Core.Common.ErrorsAndExceptions;
using System.Net;

namespace Core.CiDi.APIs.Exceptions
{
    public class APIComunicationException : ErrorException
    {
        public APIComunicationException(string mensaje) :
    base("Error de comunicación con CiDi", mensaje, null, (int)HttpStatusCode.BadRequest)
        {
            Errores.Add(mensaje);
        }
    }
}
