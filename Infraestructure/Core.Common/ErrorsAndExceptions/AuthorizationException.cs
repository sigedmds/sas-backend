﻿using System.Net;

namespace Core.Common.ErrorsAndExceptions
{
    public class AuthorizationException : ErrorException
    {


        public AuthorizationException() :
            base("No autorizado", null, null, (int)HttpStatusCode.Unauthorized)
        {
        }       

    }
}
