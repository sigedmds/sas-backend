﻿using System.Text.RegularExpressions;

namespace Core.Sanitization.Utils
{
    public static class SanitizacionUtils
    {
        public static bool CaracteresValidos(string pattern, string value)
        {
            var rx = new Regex(pattern, RegexOptions.Compiled);
            return rx.IsMatch(value);
        }

        public static bool TieneValorYCaracteresValidos(string pattern, string value)
        {
            if (string.IsNullOrEmpty(pattern))
                return false;
            var rx = new Regex(pattern, RegexOptions.Compiled);
            return rx.IsMatch(value);
        }
    }
}
