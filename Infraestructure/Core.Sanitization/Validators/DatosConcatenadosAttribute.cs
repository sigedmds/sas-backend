﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;

namespace Core.Sanitization.Validators
{
    public class DatosConcatenados : ValidationAttribute //36,-999;
    {
        public IList<string> Datos { get; set; }
        public IList<char> Separadores { get; set; }
        public IList<string> ExpresionesRegulares { get; set; }
        public IList<string> DisplayNames { get; set; }


        public DatosConcatenados(IList<char> separadores, IList<string> regex, IList<string> displayNames)
        {
            Datos = new List<string>();
            Separadores = separadores;
            ExpresionesRegulares = regex;
            DisplayNames = displayNames;
        }

        public DatosConcatenados()
        {
            Datos = new List<string>();
            Separadores = new List<char>() { ';', ',' };
            ExpresionesRegulares = new List<string>() { PatternsZanitization.IdDoceDig, PatternsZanitization.Cantidades };
            DisplayNames = new List<string>() { CamposZanitization.IdsConcatenados, CamposZanitization.IdsConcatenados };
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return ValidationResult.Success;
            if (!(value is string))
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            var datos = value as string;
            if (string.IsNullOrEmpty(datos))
                return ValidationResult.Success;
            var primerSeparador = Separadores.First();
            Datos = datos.Split();
            Separadores.Remove(primerSeparador);
            for (int j = 0; j < Datos.Count; j++)
            {
                var valores = Datos[j].Split(Separadores[j]);
                for (int z = 0; z < valores.Length; z++)
                {
                    var separadorIndex = valores[z].IndexOf(primerSeparador);
                    if (separadorIndex >= 0)
                        valores[z] = valores[z].Remove(separadorIndex);
                    if (!Regex.IsMatch(valores[z], ExpresionesRegulares[z], RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled))
                        return new ValidationResult(FormatErrorMessage(DisplayNames[z]));
                }
            }
            return ValidationResult.Success;
        }
    }
}
