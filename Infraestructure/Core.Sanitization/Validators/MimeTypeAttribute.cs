﻿using Core.Sanitization.Utils;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Core.Sanitization.Validators
{
    public class MimeTypeAttribute : ValidationAttribute
    {
        public IList<string> MimeTypes { get; set; }

        public MimeTypeAttribute(string mimestype)
        {
            MimeTypes = new List<string>();
            if (mimestype != null)
            {
                var mimes = mimestype.Split(',');
                foreach (var mime in mimes)
                    MimeTypes.Add(mime);
            }
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            var mimeTypesDescripcion = MimeTypeUtils.Obtener_MimeTypes(MimeTypes);
            if (!mimeTypesDescripcion.Any(m => m.CompareTo(value) == 0))
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            return ValidationResult.Success;
        }
    }
}
