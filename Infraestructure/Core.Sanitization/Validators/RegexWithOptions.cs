﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Core.Sanitization.Validators
{
    public class RegexWithOptions : RegularExpressionAttribute
    {
        public RegexOptions RegexOptions { get; set; }

        public RegexWithOptions(string pattern) : base(pattern)
        {
            RegexOptions = RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return ValidationResult.Success;
            if (Regex.IsMatch(value as string ?? value.ToString(), Pattern, RegexOptions))
                return ValidationResult.Success;
            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }
    }
}
