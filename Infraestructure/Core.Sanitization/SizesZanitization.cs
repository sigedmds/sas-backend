﻿namespace Core.Sanitization
{
    public abstract class SizesZanitization
    {
        public const int Uno = 1;
        public const int Dos = 2;
        public const int Tres = 3;
        public const int Cuatro = 4;
        public const int Cinco = 5;
        public const int Seis = 6;
        public const int Ocho = 8;
        public const int Diez = 10;
        public const int Once = 11;
        public const int Doce = 12;
        public const int Veinte = 20;
        public const int Veintidos = 22;
        public const int Treinta = 30;
        public const int Treintaidos = 32;
        public const int Cuarenta = 40;
        public const int Cincuenta = 50;
        public const int SesentaCuatro = 64;
        public const int Ochenta = 80;
        public const int Cien = 100;
        public const int Doscientos = 200;
        public const int DosCincuenta = 250;
        public const int Cuatricientos = 400;
        public const int Quinientos = 500;
        public const int DosMil = 2000;
        public const int TresMil = 3000;
        public const int CuatroMil = 4000;
        public const int Ultimo = 99999999;
        public const int Catorce = 14;
    }
}
