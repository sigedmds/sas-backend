﻿namespace Core.Sanitization
{
    public abstract class MsjsZanitization
    {
        public const string SinPalabrasReservadas = @"El campo {0} posee caracteres inválidos"; // acepta , y .

        public const string MaxLength = "El {0} no puede superar los {1} caracteres.";

        public const string Range = "Los valores permitidos de {0} son entre {1} y {2}.";

        public const string Requerido = "El campo {0} es requerido.";

        public const string SoloLetrasYNumeros = "El campo {0} solo permite caracteres alfánumericos.";

        public const string SoloLetras = "El campo {0}  solo permite caracteres alfábeticos.";

        public const string SoloNumeros = "El campo {0} solo permite caracteres numéricos.";

        public const string SinCaracteresEspeciales = @"El campo {0} no puede contener ninguno de los siguientes caracteres: 
                                                           `~!@#$%^&*()_°¬|+\-=?;:',.<>{{}}[]\/"; // `~!@#$%^&*()_°¬|+\-=?;:',.<>{}[]\/
        public const string NumeroDomicilio = @"El campo {0} debe ser numérico o S/N"; // `~!@#$%^&*()_°¬|+\-=?;:',.<>{}[]\/

        public const string CalleDomicilio = @"El campo {0} no puede contener ninguno de los siguientes caracteres: 
                                                           `~!@#$%^&*()_¬|+\=?;:,<>{{}}[]\/"; // `~!@#$%^&*()_°¬|+\-=?;:',.<>{}[]\/
        public const string IdInvalido = "Dato inválido.";

        public const string DatoInvalido = "El campo {0} es inválido.";       

    }
}
