﻿namespace Core.Sanitization
{
    public abstract class PatternsZanitization
    {
        public const string SinPalabrasReservadas = "^(?!.*([' ]--|select | or |<script>|<\\/script>)|'$).*$"; //no acepta que contenga las siguientes palabras: '-- ni select ni or
        public const string DecimalesReales = "^(\\d*\\.)?\\d+$";
        public const string Cantidades = "^(?!00)\\d{0,2}$";
        public const string Porcentajes = "^[0-9]+(.[0-9]+)?$";

        public const string Id = "^\\d{0,10}$";
        public const string IdOchoDig = "^\\d{0,8}$";
        public const string IdDoceDig = "^\\d{0,12}$";

        public const string CeroUno = "^[0-1]$";
        public const string Banderas = "^[S|N]$";
        public const string TrueFalse = "^[Tt]rue|[Ff]alse$";

        public const string SoloNumeros = "^[0-9]+$";
    }
}

